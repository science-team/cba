//cbeam_class.h   continuous beam analysis class - header file

/*

cbeam_class    0.3.6    07/2010    G.P.L.
 
this c++ class analyses members and forces for a given beam
sourcecode (GPL) partly based on CBA.m for octave/mathcad by C.Caprani www.colincaprani.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


usage:
    
    cBeam obj
    Constructor
    
    SetGeometry(L,E,I,R)    
    L   vector of span lengths          required, returns false if not supplied
    E   double elastic modulus          not necessarily, default=1
    I   vector of moments of inertia    not necessarily, default=1
    R   vector of constraints def/rot   not necessarily, default=(-1,0)
                    
    SetLoads(LM)            
    LM  load matrix
    every vector of loads can have the following parts (have to have the first 3)
    LM[i,0] =  number of span to be loaded
    LM[i,1] =  load type: 
                1 - uniformly distributed load
                2 - point load
                3 - partial udl
                4 - moment load
                5 - trapezoidal load (c=0 triangular)
                6 - partial triangular load (c<0 ascending, c>0 descending)
    LM[i,2] =  value of permanent load (can be zero)
    LM[i,3] =  value of live load (can be zero)     
    LM[i,4] =  start for load type 3/5/6, or point for load type 2/4
    LM[i,5] =  length for load type 3/5/6
                            
    SetGamma(g,q) 
    set the load design factors for permanent/live loads
    2 values:   gmax    qmax  
    defaults=   1.0     1.0     
    
    Solve()                 
    solves the continuous beam problem for the given geometry, loads and factors
    
    GetResults()
    returns [7,npts*nf] vector with results along the beam
    x   Mmax    Mmin    Vmax    Vmin    dmax    dmin  
    
    GetMax()
    returns [7,nf] vector with max/min results per span
    x(Mmax)     Mmax    Mmin    Vmax    Vmin    dmax    dmin  
    
    GetReaction()
    returns [2,nf+1] vector with max/min support reactions
    Rmax    Rmin 

*/

#ifndef cbeam_class
#define cbeam_class

#include <vector>
#include <cmath>

using namespace std;

class cBeam
{
  public:
	cBeam();
	~cBeam();
	
	bool SetGeometry(vector<double>, double, vector<double>, vector<double>);
	bool SetGeometry(vector<double>);
    bool SetLoads(vector< vector<double> >);
    void SetLoadFactors(double, double);
    void SetLoadFactors(double, double, double, double);
    
    vector< vector<double> > GetGeometry();
    vector< vector<double> > GetLoadsG(); 
    vector< vector<double> > GetLoadsQ(); 
	
	bool Solve();
	
	vector< vector <double> > GetResults();
	vector< vector <double> > GetMax();
	vector< vector <double> > GetReaction();
	

  private:
        
    vector<double> L;                           //spans
    double E;                                   //elastic modulus
    vector<double> I;                           //moments of inertia
    vector<double> R;                           //constraints
    
    vector< vector<double> > LMg;               //load matrix permanant loads
    vector< vector<double> > LMq;               //load matrix live loads
     
    double lfG;                                 //safety factor permanent/live loads
    double lfQ;
    double lfG_min;
    double lfQ_min;
    
    vector< vector<double> > ksys;              //stiffness matrix
    
    vector< vector<double> > k_beam(double, double, double);
    void build_ksys(vector<double>, double, vector<double>, vector<double>);
    void build_res_matrix();
    
    void cba_solve(vector< vector<double> >);
    
    vector<double> solve_glsys (vector< vector<double> >, vector<double>);
    vector<double> get_cnl(int iSpan, vector< vector<double> >);
    void set_mbr_values(int, vector<double>, vector<double>, vector< vector<double> >);
     
    int npts;                                   //precision 
    
    vector< vector<double> > Rf;                //support reactions
    vector< vector< vector <double> > > Res;    //results along the beam  
 	
};

#endif



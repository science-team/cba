//cbaGUI_drawings.h

#ifndef cbaGUI_drawings
#define cbaGUI_drawings

#include <wx/wx.h>
#include <wx/print.h>
#include <wx/printdlg.h>

#include <vector>

using namespace std;


//---------------- GraphFrame -------------------------

class GraphFrame: public wxScrolledWindow
{
 public:

    GraphFrame() {};
    virtual ~GraphFrame() {};
    
    void SetSystem(vector<double>, vector<double>, vector< vector<double> >, vector< vector<double> >);
    void SetResults(vector< vector<double> >);
    void SetSec(double mod, double sha) { Store.Wy=mod; Store.Az=sha; }

    void SetEI(bool set) { Store.EI=set; }
    void SetLF(bool set) { Store.LF=set; }
    void SetSW(bool set) { Store.SW=set; }
    
    void SetType(int);
    
    vector< vector<double> > GetResults();
    
    void Clear(); 
    
    int DrawSystem (wxDC&, wxRect, bool);
    int DrawResultText(wxDC&, wxRect, wxArrayString);
    int DrawHead(wxDC&, wxRect, wxString);
    void DrawResults(wxDC&, wxRect, int);
   
    DECLARE_EVENT_TABLE()

 private:

    void OnPaint(wxPaintEvent&);
    void OnSize(wxSizeEvent&);
    
    void DrawLoad(wxDC&, wxRect, vector<double>, double);
    void DrawArrow(wxDC&, wxPoint, int);
    void DrawTrapeze(wxDC&, wxPoint, int, int, int, int);
    int GetLoadLines();
    double GetVecPeak(vector<double>);
    
    void DrawValues(wxDC&, wxRect, int);
    void DrawStress (wxDC&, wxRect);
    void DrawBeam(wxDC&, wxRect, wxCoord, bool);
    void DrawSupport(wxDC&, wxPoint, wxCoord, int);
    void DrawDim(wxDC&, wxPoint, int, int, wxString);

    class Store_values
    {
      public:

        vector<double> L;
        vector<double> R;
        double Wy, Az;
        bool EI, LF, SW;

        vector< vector<double> > LMg;
        vector< vector<double> > LMq;
        vector< vector <double> > results;
    };

    Store_values Store;
    int type;

};


#endif




#top level makefile for cba, cba-gtk

CBADIR=$(CURDIR)/src
GTKDIR=$(CURDIR)/src/gui

all:
	( cd $(CBADIR); $(MAKE) )
	( cd $(GTKDIR); $(MAKE) )
	
clean:
	( cd $(CBADIR); $(MAKE) clean )
	( cd $(GTKDIR); $(MAKE) clean )
	
install:
	( cd $(CBADIR); $(MAKE) $(DESTDIR) install )
	( cd $(GTKDIR); $(MAKE) $(DESTDIR) install )

             
